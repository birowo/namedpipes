package main

import (
	"os"
)

func main() {
	p1p3 := "../tmpPipe"
	ss_mm_hh_len := 11
	f1l3, err := os.OpenFile(p1p3, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		println(err.Error())
		return
	}
	buf := make([]byte, ss_mm_hh_len)
	for {
		_, err = f1l3.Read(buf)
		if err != nil {
			println(err.Error())
			return
		}
		println(string(buf))
	}
}
