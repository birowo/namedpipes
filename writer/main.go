package main

import (
	"errors"
	"os"
	"syscall"
	"time"
)

func main() {
	p1p3 := "../tmpPipe"
	ss_mm_hh := "03:04:05 PM"
	ss_mm_hh_len := len(ss_mm_hh)
	_10sec := 10 * time.Second
	_, err := os.Stat(p1p3)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			syscall.Mkfifo(p1p3, 0666)
		} else {
			println(err.Error())
			return
		}
	}
	f1l3, err := os.OpenFile(p1p3, os.O_RDWR, os.ModeNamedPipe)
	if err != nil {
		println(err.Error())
		return
	}
	buf := make([]byte, ss_mm_hh_len)
	for {
		copy(buf, time.Now().Format(ss_mm_hh))
		f1l3.Write(buf)
		if err != nil {
			println(err.Error())
			return
		}
		time.Sleep(_10sec)
	}
}
