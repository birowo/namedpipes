# namedpipes

#GOLANG #LINUX

namedpipes exercise

reference: https://stackoverflow.com/questions/39407592/named-pipes-in-go-for-both-windows-and-linux

run ./writer first on terminal then run ./reader on other terminal :

![namedpipes_linux](https://user-images.githubusercontent.com/21541959/139865785-51c53dd1-b7a2-4f19-a235-547f11549d02.png)
